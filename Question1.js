const findTriplets = givenArray => {
	let tripletFound = [];

	for (i = 0; i < givenArray.length; i++) {
		for (j = i + 1; j < givenArray.length; j++) {
			for (k = j + 1; k < givenArray.length; k++) {
				if (givenArray[i] + givenArray[j] + givenArray[k] == 0) {
					found = [givenArray[i], givenArray[j], givenArray[k]];
					tripletFound.push(found);
				}
			}
		}
	}

	return tripletFound;
};

console.log(findTriplets([-1, 0, 1, 2, -1, -4]));
