#Grit Systems Engineering Test

### How to test

## Question 1

> Change the _findTriplet_ value within the `console.log()` in `Line 8`

## Question 2

> Change the _Fib_ value within the `console.log()` value from `Line 13 - Line 16`

## Question 3

> Run the `index.html` in your browser
